const models = require('../models/index');
const Modals = models.cart;
const ExtrasCart = models.cart_extras;
const VariationCart = models.cart_variation;
const FoodCart = models.cart_food;
const User = models.users;
const Stores = models.stores;
var request = require("request");

const CartController = () => {
  
  const insert = async (req, res) => {
    const { body } = req;    
      try {
        const datas = await Modals.create({
          user_id: body.user_id,
          store_id: body.store_id, 
          qty: body.qty,
          price: body.price,
          vat: body.vat,
          delivery_fee: body.delivery_fee,
          status: body.status
        });        
        return res.status(200).json({ datas });
      } catch (err) {
        console.log('Error',err)
        return res.status(500).json({ msg: err });
      }    
  };

  const get = async (req, res) => {
    try {
      const datas = await Modals.find({
        include: [{ all: true }],
        where: {
          id: req.param('id')
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };        

  const getAll = async (req, res) => {
    try {
      const datas = await Modals.findAll({
        include: [{ all: true }],
      });
      return res.status(200).json({ datas });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };
  
  const getUsersCart = async (req, res) => {
    try {
      const datas = await Modals.findOne({            
        where:{
          user_id: req.param('user_id'),          
          status: 0
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const checkCart = async (req, res) => {
    try {
      const datas = await Modals.find({        
        where: {
          user_id: req.param('user_id'),
          store_id: req.param('store_id'),
          status: 0
        }
      });
      const restDatas = await Modals.find({        
        where: {
          user_id: req.param('user_id'),          
          status: 0
        }
      });
      if(datas == null){
        return res.status(200).json({ datas: false,detail: restDatas });
      }else{
        return res.status(200).json({ datas: true,detail: restDatas });
      }
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };
  
  const checkUsersCart = async (req, res) => {
    try {
      const datas = await Modals.findOne({            
        where:{
          user_id: req.param('user_id'),
          store_id: req.param('store_id'),
          status: 0
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const update = async (req, res) => {
    try {
      Modals.findOne({
        where: {
          id: req.body.id,
        },
      })
          .then((pet) => {
            if(pet == null){
              res.status(400).json({ msg: 'Wrong Input' });
            }else{
              pet.updateAttributes(JSON.parse(req.body.data)).then((response) =>
                  res.status(200).json({ response }));
            }
          });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const remove = async (req, res) =>{
    try {
      const user = Modals.destroy({
        where: {
          id: req.param('id')
        },
      }).then(function(rowDeleted){
        if(rowDeleted != 0){
          return res.status(200).json({ status : 200, data: rowDeleted, msg : 'Deleted successfully.' });
        }else{
          return res.status(400).json({ status : 400, error: rowDeleted, msg : 'Error deleting' });
        }
      });
    } catch (err) {
      return res.status(500).json({ status : 500, error: err, msg : 'Error deleting' });
    }
  }     

  return {
    getAll,
    insert,
    get,
    update,
    remove,      
    getUsersCart,
    checkUsersCart,
    checkCart
  };
};
module.exports = CartController;
