const models = require('../models/index');
const Modals = models.cart_extras;
var request = require("request");

const CartExtraController = () => {
  
  const insert = async (req, res) => {
    const { body } = req;    
      try {
        const datas = await Modals.create({
          food_extra_id: body.food_extra_id,
          cart_id: body.cart_id,           
          food_id: body.food_id,           
          order_id: body.order_id,
          status: body.status
        });        
        return res.status(200).json({ datas });
      } catch (err) {
        console.log('Error',err)
        return res.status(500).json({ msg: err });
      }    
  };

  const get = async (req, res) => {
    try {
      const datas = await Modals.find({
        include: [{ all: true }],
        where: {
          id: req.param('id')
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };
  
  const getExtraCartByCart = async (req, res) => {
    try {
      const datas = await Modals.findAll({
        include: [{ all: true }],
        where: {
          cart_id: req.param('id')
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };  

  const getAll = async (req, res) => {
    try {
      const datas = await Modals.findAll({
        include: [{ all: true }],
      });
      return res.status(200).json({ datas });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const update = async (req, res) => {
    try {
      Modals.findOne({
        where: {
          id: req.body.id,
        },
      })
          .then((pet) => {
            if(pet == null){
              res.status(400).json({ msg: 'Wrong Input' });
            }else{
              pet.updateAttributes(JSON.parse(req.body.data)).then((response) =>
                  res.status(200).json({ response }));
            }
          });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const remove = async (req, res) =>{
    try {
      const user = Modals.destroy({
        where: {
          id: req.param('id')
        },
      }).then(function(rowDeleted){
        if(rowDeleted != 0){
          return res.status(200).json({ status : 200, data: rowDeleted, msg : 'Deleted successfully.' });
        }else{
          return res.status(400).json({ status : 400, error: rowDeleted, msg : 'Error deleting' });
        }
      });
    } catch (err) {
      return res.status(500).json({ status : 500, error: err, msg : 'Error deleting' });
    }
  }  

  return {
    getAll,
    insert,
    get,
    update,
    remove,  
    getExtraCartByCart,     
  };
};
module.exports = CartExtraController;
