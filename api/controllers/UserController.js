const models = require('../models/index');
const User = models.users;
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
var request = require("request");

const UserController = () => {

  const test = async (req, res) =>{
    try{
      const tst = req.body.test;
      return res.status(200).json({ tst });
    }catch (err) {
      return res.status(500).json({ msg: err });
    }
  }
  const register = async (req, res) => {
    const { body } = req;      
    try { 
      let profile;                 
        if(req.body.profile == ''){
          profile = '22a18276e08963d8d4b14ffcd9bdbefc1601459526676.png'
        } else{
          profile = req.body.profile
        }     
        const datas = await User.create({
          name: body.name,          
          email: body.email,
          mobile: body.mobile,
          password: bcryptService().generatePassword(body.password),
          profile_picture: profile,
          notification_id: body.notification_id,          
          google_token: body.google_token,
          fb_token: body.fb_token,          
          status: body.status,
          lat:body.lat,
          lng:body.lng,
          store_id:body.store_id,
        })            
        const token = authService().issue({ id: datas.id });
        return res.status(200).json({ token, datas });        
              
      } catch (err) {
        console.log('Error',err)
        return res.status(500).json({ msg: err });
      }    
  };

  const verify = async (req, res) => {
    try {
      const user = await User
          .findOne({
            where: {
              email: req.body.email,
            },
          });
      if(user == null){
        return res.status(200).json({ msg: 'Email Not Exist' });
      }else{
        return res.status(400).json({ msg: 'Email Already Exist' });
      }
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const login = async (req, res) => {
    const { email, password } = req.body;
    if (email && password) {
      try {
        const user = await User
            .findOne({
              where: {
                email
              },
            });          
        if (!user) {
          return res.status(400).json({ status:400,msg: 'Bad Request: User not found' });
        }
        // console.log('***',user)
        console.log('->',password)
        console.log('->',bcryptService().comparePassword(password, user.password))

        if (bcryptService().comparePassword(password, user.password)) {
          const token = authService().issue({ id: user.id });
          return res.status(200).json({ status:200,token, user });
        }

        return res.status(401).json({ status:400,msg: 'Unauthorized' });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error',error : err });
      }
    }    
  }

  const getUser = async (req, res) => {
    try {
      const user = await User.find({
        where: {
          id: req.param('id')
        }
      });
      return res.status(200).json({ user: user });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const getAll = async (req, res) => {
    try {
      const users = await User.findAll({

      });
      return res.status(200).json({ users });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };
  
  const getAllDeliveryBoy = async (req, res) => {
    try {
      const users = await User.findAll({
        where:{
          user_type:4
        }
      });
      return res.status(200).json({ users });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const updateUser = async (req, res) => {
    try {
      User.findOne({
        where: {
          id: req.body.id,
        },
      })
          .then((pet) => {
            if(pet == null){
              res.status(400).json({ status:400,msg: 'Wrong Input' });
            }else{
              pet.updateAttributes(JSON.parse(req.body.data)).then((response) =>
                  res.status(200).json({ status:200,user: response }));
            }
          });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const changePassword = async (req, res) => {
    try {
      const user = await User
          .findOne({
            where: {
              id: req.body.id,
            },
          });
      if (bcryptService().comparePassword(req.body.old_password, user.password)) {
        const NewPassword = bcryptService().generatePassword(req.body.new_password);
        console.log(NewPassword);
        User.update({ password: NewPassword }, { where: { id: req.body.id } })
            .then((rowsUpdated) => {
              res.status(200).json({ user: rowsUpdated });
            });
      } else {
        res.status(400).json({ msg: 'Old Password is Wrong' });
      }
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const deleteUser = async (req, res) =>{
    try {
      const user = User.destroy({
        where: {
          id: req.param('user_id')
        },
      }).then(function(rowDeleted){
        if(rowDeleted != 0){
          return res.status(200).json({ status : 200, data: rowDeleted, msg : 'Deleted user successfully.' });
        }else{
          return res.status(400).json({ status : 400, error: rowDeleted, msg : 'Error deleting User' });
        }
      });
    } catch (err) {
      return res.status(500).json({ status : 500, error: err, msg : 'Error deleting user' });
    }
  }

  const sendEmail = async (req, res) => {
    var options = { method: 'POST',
      url: 'https://api.sendgrid.com/v3/mail/send',
      headers:
          { 'content-type': 'application/json',
            authorization: 'Bearer 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' },
      body:
          { personalizations: [
              { to:[
                  { email: req.body.email }
                ],
                dynamic_template_data: JSON.parse(req.body.results),
                subject: req.body.subject,
              }
            ],
            from: { email: 'divyrajchavda249@gmail.com' },
            template_id: 'd-e80b40a831ff46518c3801edda279981' },
      json: true };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      return res.status(200).json({ msg:'email send successfully' });
      console.log(body);
    });
  }

  const sendEmailInvoice = async (req, res) => {
    // console.log(JSON.parse(req.body.info))
    // console.log(JSON.parse(req.body.results))
    var options = { method: 'POST',
      url: 'https://api.sendgrid.com/v3/mail/send',
      headers:
          { 'content-type': 'application/json',
            authorization: 'Bearer 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' },
      body:
          { personalizations: [
              { to:[
                  { email: req.body.email }
                ],
                dynamic_template_data: {
                  order:JSON.parse(req.body.results),
                  info:JSON.parse(req.body.info)
                },
                subject: req.body.subject,
              }
            ],
            from: { email: 'divyrajchavda249@gmail.com' },
            template_id: 'd-d47933cb64fd4c028f39d09ec01ca24c' },
      json: true };
    console.log(options.body.personalizations[0].dynamic_template_data)
    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      return res.status(200).json({ msg:'email send successfully' });
      console.log(body);
    });
  }

  return {
    getAll,
    register,
    verify,
    login,
    getUser,
    updateUser,
    changePassword,
    deleteUser,
    test,
    sendEmail,
    sendEmailInvoice,
    getAllDeliveryBoy
  };
};
module.exports = UserController;
