const models = require('../models/index');
const Modals = models.cart_food;
const VariationCart = models.cart_variation;
const ExtrasCart = models.cart_variation;
const Stores = models.stores;
const Media = models.media;
const Food = models.food;
var request = require("request");

const CartFoodController = () => {
  
  const insert = async (req, res) => {
    const { body } = req;    
      try {
        const datas = await Modals.create({
          food_id: body.food_id,
          cart_id: body.cart_id,           
          total: body.total,           
          qty: body.qty,           
          status: body.status
        });        
        return res.status(200).json({ datas });
      } catch (err) {
        console.log('Error',err)
        return res.status(500).json({ msg: err });
      }    
  };
  
  const get = async (req, res) => {
    try {
      const datas = await Modals.find({
        include: [{ all: true}],
        where: {
          id: req.param('id')
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };
  
  const getFoodCartByCart = async (req, res) => {
    try {
      const datas = await Modals.findAll({
        include: [{ all: true },{model:Food,include:[{all: true}]}],
        where: {
          cart_id: req.param('id')
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };  

  const getAll = async (req, res) => {
    try {
      const datas = await Modals.findAll({
        include: [{ all: true }],
      });
      return res.status(200).json({ datas });
    } catch (err) {
      console.log(err)
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const update = async (req, res) => {
    try {
      Modals.findOne({
        where: {
          id: req.body.id,
        },
      })
          .then((pet) => {
            if(pet == null){
              res.status(400).json({ msg: 'Wrong Input' });
            }else{
              pet.updateAttributes(JSON.parse(req.body.data)).then((response) =>
                  res.status(200).json({ response }));
            }
          });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const remove = async (req, res) =>{
    try {
      const user = Modals.destroy({
        where: {
          id: req.param('id')
        },
      }).then(function(rowDeleted){
        if(rowDeleted != 0){
          return res.status(200).json({ status : 200, data: rowDeleted, msg : 'Deleted successfully.' });
        }else{
          return res.status(400).json({ status : 400, error: rowDeleted, msg : 'Error deleting' });
        }
      });
    } catch (err) {
      return res.status(500).json({ status : 500, error: err, msg : 'Error deleting' });
    }
  }  
  
  const removeFoodCartData = async (req, res) =>{
    try {
      const foodcarts = Modals.destroy({
        where: {
          id: req.param('foodcart_id'),     
          food_id: req.param('food_id'),
          cart_id: req.param('cart_id')
        },
      }).then(function(rowDeleted){
        if(rowDeleted != 0){
          const variationcarts = VariationCart.destroy({
            where: {
              food_id: req.param('foodcart_id'),
              cart_id: req.param('cart_id')
            },
          }).then(function(rowDeleted){
            if(rowDeleted != 0){
              const extracarts = ExtrasCart.destroy({
                where: {
                  food_id: req.param('foodcart_id'),
                  cart_id: req.param('cart_id')
                },
              }).then(function(rowDeleted){
                if(rowDeleted != 0){
                  return res.status(200).json({ status : 200, data: rowDeleted, msg : 'Deleted successfully.' });
                }else{
                  return res.status(400).json({ status : 400, error: rowDeleted, msg : 'Error deleting Extras Cart' });
                }
              });              
            }else{
              return res.status(400).json({ status : 400, error: rowDeleted, msg : 'Error deleting Variation Cart' });
            }
          }); 
          return res.status(200).json({ status : 200, data: rowDeleted, msg : 'Deleted successfully.' });         
        }else{          
          return res.status(400).json({ status : 400, error: rowDeleted, msg : 'Error deleting Food Cart' });
        }
      });
                      
    } catch (err) {
      return res.status(500).json({ status : 500, error: err, msg : 'Error deleting' });
    }
  }  

  return {
    getAll,
    insert,
    get,
    update,
    remove,  
    getFoodCartByCart,
    removeFoodCartData     
  };
};
module.exports = CartFoodController;
