const models = require('../models/index');
const Modals = models.stores;
var request = require("request");
const Geo = require('geo-nearby');

const StoreController = () => {
  
  const insert = async (req, res) => {
    const { body } = req;    
      try {
        const datas = await Modals.create({          
          title: body.title,
          description: body.description,                    
          user_id: body.user_id, 
          media_id: body.media_id,                    
          lat: body.lat, 
          lng: body.lng, 
          rating: body.rating, 
          is_open: body.is_open, 
          is_verified: body.is_verified, 
          status: body.status,
        });        
        return res.status(200).json({ datas });
      } catch (err) {
        console.log('Error',err)
        return res.status(500).json({ msg: err });
      }    
  };

  const get = async (req, res) => {
    try {
      const datas = await Modals.find({
        include: [{ all: true,nested: true }],
        where: {
          id: req.param('id')
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };
  
  const getCategoryByStore = async (req, res) => {
    try {
      const datas = await Modals.findAll({
        include: [{ all: true,nested: true }],
        where: {
          store_id: req.param('id')
        }
      });
      return res.status(200).json({ datas });
    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const getAll = async (req, res) => {
    try {
      const datas = await Modals.findAll({
        include: [{ all: true,nested: true }],
        order:[['id','ASC']]
      });
      return res.status(200).json({ datas });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const update = async (req, res) => {
    try {
      Modals.findOne({
        where: {
          id: req.body.id,
        },
      })
          .then((pet) => {
            if(pet == null){
              res.status(400).json({ msg: 'Wrong Input' });
            }else{
              pet.updateAttributes(JSON.parse(req.body.data)).then((response) =>
                  res.status(200).json({ response }));
            }
          });
    } catch (err) {
      return res.status(500).json({ msg: 'Internal server error' });
    }
  };

  const remove = async (req, res) =>{
    try {
      const user = Modals.destroy({
        where: {
          id: req.param('id')
        },
      }).then(function(rowDeleted){
        if(rowDeleted != 0){
          return res.status(200).json({ status : 200, data: rowDeleted, msg : 'Deleted successfully.' });
        }else{
          return res.status(400).json({ status : 400, error: rowDeleted, msg : 'Error deleting' });
        }
      });
    } catch (err) {
      return res.status(500).json({ status : 500, error: err, msg : 'Error deleting' });
    }
  } 
  
  const getNearByStores = async(req, res) =>{

    const initial_lat_lng = [];
    const temp = [];
    const array = [];

    try {
      const cabs = await Modals.findAll({
        include:[{ all:true, nested: true}],        
      });      

      if(cabs.length <= 0 ){
        return res.status(404).json({ msg: 'No nearby store found.' });
      }else{
        for (let i = 0; i < cabs.length; i++) {
          initial_lat_lng.push({ lat: cabs[i].lat, lon: cabs[i].lng, __id: cabs[i].id });
        }
  
        const geo = new Geo(initial_lat_lng, { setOptions: { id: '__id', lat: 'lat', lon: 'lon' } });
  
        const finalDataSet = geo.nearBy(req.body.lat, req.body.lng, req.body.radius);
  
        finalDataSet.forEach((data) => {
          const picked = cabs.find((cabs) => cabs.id === data.i);
          temp.push(picked);
        });
        return res.status(200).json({ temp });
      }

    } catch (err) {
      console.log(err);
      return res.status(500).json({ msg: 'Internal server error' });
    }
  }

  return {
    getAll,
    insert,
    get,
    update,
    remove,
    getCategoryByStore,
    getNearByStores  
  };
};
module.exports = StoreController;
