/* jshint indent: 2 */

const { DOUBLE } = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  const variations = sequelize.define('variations', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(50),
      allowNull: true
    },    
    store_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    }    
  }, {
    tableName: 'variations',
    timestamps: false
  });

//   food_variation.associate = function (models) {
//     food_variation.belongsTo(models.users, { foreignKey: 'user_id', onDelete: 'cascade' })
//     food_variation.belongsTo(models.store, { foreignKey: 'store_id', onDelete: 'cascade' })
//   }

  return variations;
};
