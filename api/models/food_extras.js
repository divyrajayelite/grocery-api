/* jshint indent: 2 */

const { FLOAT, DOUBLE } = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  const food_extras = sequelize.define('food_extras', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    food_id: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    media_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    store_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    extras_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    title: {
      type: DataTypes.STRING(45),
      allowNull: true
    },    
    price: {
      type: DOUBLE,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    }    
  }, {
    tableName: 'food_extras',
    timestamps: false
  });

  food_extras.associate = function (models) {
    food_extras.belongsTo(models.media, { foreignKey: 'media_id',as:'media', onDelete: 'cascade' })
    food_extras.belongsTo(models.food, { foreignKey: 'food_id', onDelete: 'cascade' })
    food_extras.belongsTo(models.extras, { foreignKey: 'extras_id', onDelete: 'cascade' })
    food_extras.belongsTo(models.stores, { foreignKey: 'store_id', onDelete: 'cascade' })
  }

  return food_extras;
};
