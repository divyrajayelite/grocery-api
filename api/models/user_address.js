/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const user_address = sequelize.define('user_address', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },  
    title: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    street: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    zip_code: {
      type: DataTypes.STRING(45),
      allowNull: true
    },    
    country: {
      type: DataTypes.STRING(45),
      allowNull: true
    },    
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },    
  }, {
    tableName: 'user_address',
    timestamps: false
  });

  user_address.associate = function (models) {
    user_address.belongsTo(models.users, { foreignKey: 'user_id', onDelete: 'cascade' })    
  }

  return user_address;
};
