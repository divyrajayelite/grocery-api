
module.exports = function(sequelize, DataTypes) {
  const users = sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: true
    },    
    email: {
      type: DataTypes.TEXT,
      allowNull: true
    },    
    password: {
      type: DataTypes.TEXT,
      allowNull: true
    },    
    mobile: {
      type: DataTypes.TEXT,
      allowNull: true
    },    
    profile_picture: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    notification_id: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    google_token: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    fb_token: {
      type: DataTypes.TEXT,
      allowNull: true
    },           
    lat: {
      type: DataTypes.TEXT,
      allowNull: true
    },           
    lng: {
      type: DataTypes.TEXT,
      allowNull: true
    },           
    store_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },           
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
  }, {
    tableName: 'users',
    timestamps: false
  });

  return users;
};
