const { DOUBLE } = require("sequelize");

module.exports = function(sequelize, DataTypes) {
    const orders = sequelize.define('orders', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      store_id: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      driver_id: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      cart_id: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      user_address_id: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      payment_id: {
        type: DataTypes.TEXT,
        allowNull: true
      },    
      tax: {
        type: DOUBLE,
        allowNull: true
      },
      delivery_fee: {
        type: DOUBLE,
        allowNull: true
      },
      status: {
        type: DataTypes.INTEGER(4),
        allowNull: true
      },
      order_date:{
        type: DataTypes.TEXT,
        allowNull: true
      },
      order_time:{
        type: DataTypes.TEXT,
        allowNull: true
      }
    }, {
      tableName: 'orders',
      timestamps: false
    });
  
    // orders.associate = function (models) {
    //   orders.belongsTo(models.users, { foreignKey: 'user_id', as: 'user', onDelete: 'cascade' })
    //   orders.belongsTo(models.user_address, { foreignKey: 'user_id', as: 'user_address', onDelete: 'cascade' })
    //   orders.belongsTo(models.restaurants, { foreignKey: 'restaurant_id', onDelete: 'cascade' })    
    //   orders.belongsTo(models.users, { foreignKey: 'driver_id',as: 'driver', onDelete: 'cascade' })
    //   orders.belongsTo(models.cart, { foreignKey: 'cart_id', onDelete: 'cascade' })
    //   orders.belongsTo(models.user_address, { foreignKey: 'user_address_id', onDelete: 'cascade' })
    //   orders.hasMany(models.extras_cart, { foreignKey: 'cart_id', onDelete: 'cascade' })
    //   orders.hasMany(models.variation_cart, { foreignKey: 'cart_id', onDelete: 'cascade' }) 
    //   orders.hasMany(models.food_cart, { foreignKey: 'cart_id', onDelete: 'cascade' })             
    // }
  
    return orders;
  };
  