/* jshint indent: 2 */

const { FLOAT, DOUBLE } = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  const cart_food =  sequelize.define('cart_food', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    cart_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },      
    qty: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    total: {
      type: DOUBLE,
      allowNull: true
    },    
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    }    
  }, {
    tableName: 'cart_food',
    timestamps: false
  });

  cart_food.associate = function (models) {
    cart_food.belongsTo(models.food, { foreignKey: 'food_id', onDelete: 'cascade' })    
    cart_food.belongsTo(models.cart, { foreignKey: 'cart_id', onDelete: 'cascade' })
    cart_food.hasMany(models.cart_extras, { foreignKey: 'cart_id', onDelete: 'cascade' })
    cart_food.hasMany(models.cart_variation, { foreignKey: 'cart_id', onDelete: 'cascade' })    
  }

  return cart_food
};
