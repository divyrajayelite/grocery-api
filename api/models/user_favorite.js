/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const user_favorite = sequelize.define('user_favorite', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },  
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    food_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },    
  }, {
    tableName: 'user_favorite',
    timestamps: false
  });

  user_favorite.associate = function (models) {
    user_favorite.belongsTo(models.users, { foreignKey: 'user_id', onDelete: 'cascade' })    
    user_favorite.belongsTo(models.food, { foreignKey: 'food_id', onDelete: 'cascade' })    
  }

  return user_favorite;
};
