/* jshint indent: 2 */

const { DOUBLE } = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  const extras = sequelize.define('extras', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(50),
      allowNull: true
    },    
    store_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    }    
  }, {
    tableName: 'extras',
    timestamps: false
  });

  return extras;
};
