
module.exports = function(sequelize, DataTypes) {
  const cart_variation =  sequelize.define('cart_variation', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    cart_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    food_variation_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }, 
    food_id:{
      type: DataTypes.INTEGER(11),
      allowNull: true
    },   
    order_id:{
      type: DataTypes.INTEGER(11),
      allowNull: true
    },   
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    }    
  }, {
    tableName: 'cart_variation',
    timestamps: false
  });

  // cart_variation.associate = function (models) {
  //   cart_variation.belongsTo(models.food_variation, { foreignKey: 'variation_id', onDelete: 'cascade' })
  //   cart_variation.belongsTo(models.cart, { foreignKey: 'cart_id', onDelete: 'cascade' })    
  // }

  return cart_variation
};
