/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const categories = sequelize.define('categories', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },  
    title: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    media_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    store_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },    
  }, {
    tableName: 'categories',
    timestamps: false
  });

  categories.associate = function (models) {
    categories.belongsTo(models.media, { foreignKey: 'media_id',as: 'media', onDelete: 'cascade' })
    categories.belongsTo(models.stores, { foreignKey: 'store_id', onDelete: 'cascade' })    
  }

  return categories;
};
