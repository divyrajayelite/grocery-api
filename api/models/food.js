/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const food = sequelize.define('food', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },  
    title: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    media_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    store_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    price: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },    
    cal: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },    
  }, {
    tableName: 'food',
    timestamps: false
  });

  food.associate = function (models) {
    food.belongsTo(models.media, { foreignKey: 'media_id',as: 'media', onDelete: 'cascade' })
    food.belongsTo(models.stores, { foreignKey: 'store_id', onDelete: 'cascade' })    
  }

  return food;
};
