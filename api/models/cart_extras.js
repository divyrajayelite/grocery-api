
module.exports = function(sequelize, DataTypes) {
    const cart_extras =  sequelize.define('cart_extras', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      cart_id: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      food_extra_id: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },   
      food_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      order_id:{
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      status: {
        type: DataTypes.INTEGER(4),
        allowNull: true
      }    
    }, {
      tableName: 'cart_extras',
      timestamps: false
    });
  
    // cart_extras.associate = function (models) {
    //     cart_extras.belongsTo(models.food_extras, { foreignKey: 'extra_id', onDelete: 'cascade' })
    //     cart_extras.belongsTo(models.cart, { foreignKey: 'cart_id', onDelete: 'cascade' })        
    // }
  
    return cart_extras
  };
  