const { DOUBLE } = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  const cart =  sequelize.define('cart', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    store_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    qty: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },    
    price: {
      type: DOUBLE,
      allowNull: true
    },    
    vat: {
      type: DOUBLE,
      allowNull: true
    },    
    delivery_fee: {
      type: DOUBLE,
      allowNull: true
    }    
  }, {
    tableName: 'cart',
    timestamps: false
  });

  cart.associate = function (models) {
    cart.belongsTo(models.users, { foreignKey: 'user_id', onDelete: 'cascade' })
    cart.belongsTo(models.stores, { foreignKey: 'store_id', onDelete: 'cascade' })
    cart.hasMany(models.cart_extras, { foreignKey: 'cart_id', onDelete: 'cascade' })
    cart.hasMany(models.cart_variation, { foreignKey: 'cart_id', onDelete: 'cascade' })
    cart.hasMany(models.cart_food, { foreignKey: 'cart_id', onDelete: 'cascade' })    
  }

  return cart
};
