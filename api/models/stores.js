/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const stores = sequelize.define('stores', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },  
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },       
    title: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    media_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },       
    rating: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },    
    lat: {
      type: DataTypes.TEXT,
      allowNull: true
    },    
    lng: {
      type: DataTypes.TEXT,
      allowNull: true
    },    
    is_open: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },    
    is_verified: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },    
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },    
  }, {
    tableName: 'stores',
    timestamps: false
  });

  stores.associate = function (models) {
    stores.belongsTo(models.media, { foreignKey: 'media_id',as: 'media', onDelete: 'cascade' })    
    stores.belongsTo(models.users, { foreignKey: 'user_id', onDelete: 'cascade' })    
  }

  return stores;
};
