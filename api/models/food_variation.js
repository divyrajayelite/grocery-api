/* jshint indent: 2 */

const { DOUBLE } = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  const food_variation = sequelize.define('food_variation', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    price: {
      type: DOUBLE,
      allowNull: true
    },
    food_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    media_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    variation_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    store_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },    
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    }    
  }, {
    tableName: 'food_variation',
    timestamps: false
  });

  food_variation.associate = function (models) {
    food_variation.belongsTo(models.media, { foreignKey: 'media_id',as:'media', onDelete: 'cascade' })
    food_variation.belongsTo(models.food, { foreignKey: 'food_id', onDelete: 'cascade' })
    food_variation.belongsTo(models.variations, { foreignKey: 'variation_id', onDelete: 'cascade' })
    food_variation.belongsTo(models.stores, { foreignKey: 'store_id', onDelete: 'cascade' })
  }

  return food_variation;
};
