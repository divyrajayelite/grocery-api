var OneSignal = require('onesignal-node');

const sendNoti = () => {
	const sendGeneralNotification = ( title, msg ) =>{
		const myClient = new OneSignal.Client({
			userAuthKey: 'MGQ1N2U2YWYtNzU5MC00YTRkLWE4YTEtYmNhNWNkMTRkYzll',
			app: { appAuthKey: 'MGQ1N2U2YWYtNzU5MC00YTRkLWE4YTEtYmNhNWNkMTRkYzll', appId: '1f8314e5-9a86-47a7-a6aa-66e81e1ed7a4' }
		});
		const firstNotification = new OneSignal.Notification({
			contents: {
				en: msg,
			},
			headings: {
				en: title,
			},
			included_segments: ["Active Users", "Inactive Users"]
		});
		return myClient.sendNotification(firstNotification);
	}

	const sendDeviceSpecificNotification = ( token, msg ) => {
		console.log('Service',token);
		console.log('Service',msg);
		const myClient = new OneSignal.Client({
			userAuthKey: 'MGQ1N2U2YWYtNzU5MC00YTRkLWE4YTEtYmNhNWNkMTRkYzll',
			app: { appAuthKey: 'MGQ1N2U2YWYtNzU5MC00YTRkLWE4YTEtYmNhNWNkMTRkYzll', appId: '1f8314e5-9a86-47a7-a6aa-66e81e1ed7a4' }
		});
		const firstNotification = new OneSignal.Notification({
			contents: {
				en: msg,
			},
			include_player_ids: [token]
		});
		return myClient.sendNotification(firstNotification);
	}

	return {
		sendGeneralNotification,
		sendDeviceSpecificNotification
	};
  };

module.exports = sendNoti;

