const fs = require('fs');

const audioVideoService = () => {
	
	const removeAudio = async (video) =>{

		var spawn = require('child_process').spawn;

	    var cmd = 'ffmpeg';

	    var args = [
	        '-i', '/var/www/html/cmt_api_node/uploads/temp/'+video,
	        '-vcodec', 'copy', 
	        '-an', '/var/www/html/cmt_api_node/uploads/temp/output.mp4',
	    ];

	    var proc = spawn(cmd, args);

	    proc.stderr.on('data', function(data) {
	        console.log(data);
	    });

	    function completed() {
		  return new Promise(resolve => {
		    proc.stderr.on('close', function(data) {
			    resolve(1)
		    });
		  });
		}

		return await completed();

	}

	const removeTemp = async ( file ) =>{

		
		var spawn = require('child_process').spawn;

	    var cmd = 'rm';

	    var args = [
	        '-rf', '/var/www/html/cmt_api_node/uploads/temp/'+file,
	    ];

	    var proc = spawn(cmd, args);

	    proc.stdout.on('data', function (data) {
		  resolve(200)
		});

		proc.stderr.on('data', function (data) {
		  resolve(500)
		});

		proc.on('exit', function (code) {
		  console.log("finished");
		});

	    function completed() {
		  return new Promise(resolve => {
		    proc.on('exit', function(data) {
			    resolve(200)
		    });
		  });
		}

		return await completed();

	}

	const removeVideo =async  (file) =>{
		var spawn = require('child_process').spawn;

		var cmd = 'rm';

		var args = [
			'-rf', '/var/www/html/cmt_api_node/uploads/videos/'+file,
		];

		var proc = spawn(cmd, args);

		proc.stdout.on('data', function (data) {
			resolve(200)
		});

		proc.stderr.on('data', function (data) {
			resolve(500)
		});

		proc.on('exit', function (code) {
			console.log("finished");
		});

		function completed() {
			return new Promise(resolve => {
				proc.on('exit', function(data) {
					resolve(200)
				});
			});
		}

		return await completed();
	}

	const mergeAudio = async ( audio ) =>{

		var final_file_name;
		this.final_file_name = Date.now();

		console.log(final_file_name);
		console.log(this.final_file_name);

		var spawn = require('child_process').spawn;

		var cmd = 'ffmpeg';

		var args = [
	        '-i', '/var/www/html/cmt_api_node/uploads/temp/output.mp4',
	        '-i', '/var/www/html/cmt_api_node/uploads/temp/'+audio,
	        '-c:v', 'copy',
	        '-c:a', 'aac',
	        '-strict', 'experimental',
	        '-y', '/var/www/html/cmt_api_node/uploads/videos/'+this.final_file_name+'.mp4'
	    ];

	    var proc2 = spawn(cmd, args);



	    function completed(file) {
		  return new Promise(resolve => {
		    proc2.stderr.on('close', function(data) {
				console.log("OKKOKOKOk");
		    	console.log(file);
		    	console.log(data);
				try {
					console.log(file);
				  	if (fs.existsSync('/var/www/html/cmt_api_node/uploads/videos/'+file+'.mp4')) {
				  	   	var util = require('util'),
					    child_process = require('child_process');

					    var exec = child_process.exec;

					    function puts(error, stdout, stderr) {
					      stdout ? console.log('stdout: ' + stdout) : null;
					      stderr ? console.log('stderr: ' + stderr) : null;
					      error ? console.log('exec error: ' + error) : null;
					      let data = { time: stdout, file: file+".mp4", status: 200 };
					      resolve(data);
					    }
					    exec("ffmpeg -i /var/www/html/cmt_api_node/uploads/videos/"+file+".mp4 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//", puts);

					    var exec2 = child_process.exec2;

					    function puts2(error, stdout, stderr) {
					      stdout ? console.log('stdout: ' + stdout) : null;
					      stderr ? console.log('stderr: ' + stderr) : null;
					      error ? console.log('exec error: ' + error) : null;
					    }
					    exec("rm -rf /var/www/html/cmt_api_node/uploads/temp/output.mp4", puts2);

				  	}else{
				  		let data = { status: 500, msg: "Something Went to Wrong" };
					    resolve(data);
				  	}
				} catch(err) {
				  	let data = { status: 500, msg: "Something Went to Wrong" };
					resolve(data);
				}
		    });
		  });
		}

		return await completed(this.final_file_name);
	}

	const splitVideo = async (video, time) =>{
		var spawn = require('child_process').spawn;
		console.log(video, time)
	    var cmd = 'ffmpeg';

		var final_file_name;
		this.final_file_name = Date.now();
		// ffmpeg -i largefile.mp4 -t 00:50:00 -c copy smallfile1.mp4
	    var args = [
		  '-i', '/var/www/html/cmt_api_node/uploads/videos/'+video,
		  '-t', time,
		  '-c', 'copy',
		  '/var/www/html/cmt_api_node/uploads/videos/'+this.final_file_name+'.mp4'
	    ];

	    var proc = spawn(cmd, args);

	    proc.stderr.on('data', function(data) {
	        console.log(data);
	    });

	    function completed(file) {
		  return new Promise(resolve => {
		    proc.stderr.on('close', function(data) {
				let msg = {  file: file+".mp4", status: 200 };
				resolve(msg);
		    });
		  });
		}

		return await completed(this.final_file_name);
	}
	
	return {
		removeAudio,
		mergeAudio,
		removeVideo,
		removeTemp,
		splitVideo
	};
  };
  
module.exports = audioVideoService;
