/**
 * third party libraries
 */
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const https = require('https');
let io = require('socket.io')(https);
var multiparty = require("multiparty");
var util = require('util');
const helmet = require('helmet');
const mapRoutes = require('express-routes-mapper');
const cors = require('cors');
var multer  = require('multer')
var crypto = require("crypto");
const mime = require('mime');
var ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
// var storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, './uploads/')
//   },
//   filename: function (req, file, cb) {
//     crypto.pseudoRandomBytes(16, function (err, raw) {
//       cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
//     });
//   }
// });
// var upload = multer({ storage: storage });

// var MultiStorage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, './uploads/temp/')
//   },
//   filename: function (req, file, cb) {
//     crypto.pseudoRandomBytes(16, function (err, raw) {
//       cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
//     });
//   }
// });
// var MulitUpload = multer({ storage: MultiStorage });

const dotenv = require('dotenv');
dotenv.config();
/**
 * server configuration
 */
const config = require('../config/');
const dbService = require('./services/db.service');
const audioVideoService = require('./services/audioVideo.service');
const auth = require('./policies/auth.policy');

// environment: development, staging, testing, production
const environment = process.env.NODE_ENV;

/**
 * express application
 */

// const server = http.Server(app);
const mappedOpenRoutes = mapRoutes(config.publicRoutes, 'api/controllers/');
const mappedAuthRoutes = mapRoutes(config.privateRoutes, 'api/controllers/');
const DB = dbService(environment, config.migrate).start();

// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());

io.on('connection', (socket) => {
  console.log(socket.id);

  socket.on('send', (message) => {
    io.to(message.room_id).emit('message', {text: message.text});
    console.log("Message =>", message)
  });

  socket.on('joinRoom', function(data){
    socket.join(data);
    console.log("joining in =>", data);
  });

  socket.on('leaveRoom', function(data){
    socket.leave(data);
    console.log("leaving in =>", data);
  });

});

// secure express app
app.use(helmet({
  dnsPrefetchControl: false,
  frameguard: false,
  ieNoOpen: false,
}));

var options = {
  key: fs.readFileSync('./server.key'),
  cert: fs.readFileSync('./primary.crt'),
  ca: fs.readFileSync('./inter.crt')
};

// parsing the request bodys
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.post('/merge', async function(req, res){
  try{
    let x = await audioVideoService().removeAudio(req.body.video);
    let y;
    if(x == 1){
      y = await audioVideoService().mergeAudio(req.body.audio);
      console.log(y);
    }
    return res.status(200).json({ status : y })
  }catch (err) {
    console.log ('error',err);
    return res.status(500).json({ msg: err });
  }
});

app.post('/splitVideo', async function(req, res){
  try{
    let x = await audioVideoService().splitVideo(req.body.video, req.body.time);
    return res.status(200).json({ status : x })
  }catch (err) {
    console.log ('error',err);
    return res.status(500).json({ msg: err });
  }
});


app.post('/removetemp', async function (req, res){
  try{
    let x = await audioVideoService().removeTemp(req.body.file);

    return res.status(200).json({ status: x })
  }catch (err){
    return res.status(500).json({ msg : err })
  }
});

app.post('/deleteVideo', async function (req, res) {
  try{
    let x = await  audioVideoService().removeVideo(req.body.file);
    return res.status(200).json({ status: x })
  }catch (e) {
    return res.status(500).json({ msg : e })
  }
})

app.use(express.static('public'));
// file upload
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
    });
  }
});
var upload = multer({ storage: storage });

app.post('/upload_file', upload.single('file'), function (req, res, next) {
  try{
    return res.status(200).json({ file: res.req.file.filename });
  }catch (err) {
    console.log(err);
    return res.status(500).json({ msg: 'Internal Server Error' });
  }
})

// app.post('/upload_mulit_file', MulitUpload.single('file'), function (req, res, next) {
//   try{
//     return res.status(200).json({ file: res.req.file.filename });
//   }catch (err) {
//     console.log(err);
//     return res.status(500).json({ msg: 'Internal Server Error' });
//   }
// })

// secure your private routes with jwt authentication middleware
app.all('/private/*', (req, res, next) => auth(req, res, next));

// fill routes for express application
app.use('/public', mappedOpenRoutes);
app.use('/private', mappedAuthRoutes);

var server = https.createServer(options, app).listen(config.port,function() {
  console.log('Tes server listening on %d, in %s mode', config.port, app.get('env'));
});
// server.listen(config.port, () => {
//   if (environment !== 'production' &&
//     environment !== 'development' &&
//     environment !== 'testing'
//   ) {
//     console.error(`NODE_ENV is set to ${environment}, but only production and development are valid.`);
//     process.exit(1);
//   }
//   return DB;
// });
