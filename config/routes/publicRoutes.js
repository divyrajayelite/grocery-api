const publicRoutes = {
    'POST /register': 'UserController.register',
    'GET /test': 'UserController.test',
    'POST /login': 'UserController.login',
    'POST /verify': 'UserController.verify',    
};

module.exports = publicRoutes;
