const privateRoutes = {

  /* Users */
  'GET /users': 'UserController.getAll',
  'GET /user': 'UserController.getUser',
  'PUT /user': 'UserController.updateUser',
  'DELETE /user': 'UserController.deleteUser',  

  // Media

  'POST /media': 'MediaController.insert',
  'GET /media': 'MediaController.get',
  'GET /medias': 'MediaController.getAll',
  'PUT /media': 'MediaController.update',
  'DELETE /media': 'MediaController.remove',
  
  // Store

  'POST /store': 'StoreController.insert',
  'GET /store': 'StoreController.get',
  'GET /stores': 'StoreController.getAll',
  'POST /getNearByStores': 'StoreController.getNearByStores',
  'PUT /store': 'StoreController.update',
  'DELETE /store': 'StoreController.remove',
  
  
  // Categories

  'POST /category': 'CategoriesController.insert',
  'GET /category': 'CategoriesController.get',
  'GET /categories': 'CategoriesController.getAll',
  'GET /categoriesByStore': 'CategoriesController.getCategoryByStore',
  'PUT /category': 'CategoriesController.update',
  'DELETE /category': 'CategoriesController.remove',
  
  // Food

  'POST /food': 'FoodController.insert',
  'GET /food': 'FoodController.get',
  'GET /foods': 'FoodController.getAll',
  'GET /foodByStore': 'FoodController.getFoodByStore',
  'PUT /food': 'FoodController.update',
  'DELETE /food': 'FoodController.remove',
  
  // User Address

  'POST /address': 'UserAddressController.insert',
  'GET /address': 'UserAddressController.get',
  'GET /addresses': 'UserAddressController.getAll',
  'GET /addressByUser': 'UserAddressController.getAddressByUser',
  'PUT /address': 'UserAddressController.update',
  'DELETE /address': 'UserAddressController.remove',
  
  
  // User Favorite

  'POST /favorite': 'UserFavoriteController.insert',
  'GET /favorite': 'UserFavoriteController.get',
  'GET /favorites': 'UserFavoriteController.getAll',
  'GET /favoriteByUser': 'UserFavoriteController.getFavoriteByUser',
  'PUT /favorite': 'UserFavoriteController.update',
  'DELETE /favorite': 'UserFavoriteController.remove',
  
  // Discounts

  'POST /discount': 'DiscountsController.insert',
  'GET /discount': 'DiscountsController.get',
  'GET /discounts': 'DiscountsController.getAll',
  'GET /discountsByStore': 'DiscountsController.getDiscountsByStore',
  'PUT /discount': 'DiscountsController.update',
  'DELETE /discount': 'DiscountsController.remove',

  // Cart

  'POST /cart': 'CartController.insert',
  'GET /cart': 'CartController.get',
  'GET /carts': 'CartController.getAll',    
  'GET /getUsersCart': 'CartController.getUsersCart',  
  'GET /checkUsersCart': 'CartController.checkUsersCart',  
  'GET /checkCart': 'CartController.checkCart',  
  'PUT /cart': 'CartController.update',
  'DELETE /cart': 'CartController.remove',
    
  // Cart Extras

  'POST /cartextra': 'CartExtraController.insert',
  'GET /cartextra': 'CartExtraController.get',
  'GET /cartextras': 'CartExtraController.getAll',  
  'GET /getExtraCartByCart': 'CartExtraController.getExtraCartByCart',  
  'PUT /cartextra': 'CartExtraController.update',
  'DELETE /cartextra': 'CartExtraController.remove',

  // // Cart Variation

  'POST /cartvariation': 'CartVariationController.insert',
  'GET /cartvariation': 'CartVariationController.get',
  'GET /cartvariations': 'CartVariationController.getAll',  
  'GET /getExtraVariationByCart': 'CartVariationController.getExtraVariationByCart',  
  'PUT /cartvariation': 'CartVariationController.update',
  'DELETE /cartvariation': 'CartVariationController.remove',
  
  // // Cart Food

  'POST /cartfood': 'CartFoodController.insert',
  'GET /cartfood': 'CartFoodController.get',
  'GET /cartfoods': 'CartFoodController.getAll',  
  'GET /getFoodCartByCart': 'CartFoodController.getFoodCartByCart',     
  'PUT /cartfood': 'CartFoodController.update',
  'DELETE /cartfood': 'CartFoodController.remove',
  'DELETE /removeFoodCartData': 'CartFoodController.removeFoodCartData',

  // Extras
  'POST /extra': 'ExtrasController.insert',
  'GET /extra': 'ExtrasController.get',
  'GET /extras': 'ExtrasController.getAll',       
  'PUT /extra': 'ExtrasController.update',
  'DELETE /extra': 'ExtrasController.remove',
  
  // Variation
  'POST /variation': 'VariationController.insert',
  'GET /variation': 'VariationController.get',
  'GET /variations': 'VariationController.getAll',       
  'PUT /variation': 'VariationController.update',
  'DELETE /variation': 'VariationController.remove',
  
  // Food Extras
  'POST /foodExtra': 'FoodExtraController.insert',
  'GET /foodExtra': 'FoodExtraController.get',
  'GET /foodExtras': 'FoodExtraController.getAll',       
  'GET /foodExtrasByFood': 'FoodExtraController.getFoodExtrasByFoodId',       
  'PUT /foodExtra': 'FoodExtraController.update',
  'DELETE /foodExtra': 'FoodExtraController.remove',
  
  // Food Variation
  'POST /foodVriation': 'FoodVariationController.insert',
  'GET /foodVriation': 'FoodVariationController.get',
  'GET /foodVriations': 'FoodVariationController.getAll',       
  'GET /foodVriationsByFood': 'FoodVariationController.getFoodVariationByFoodId',       
  'PUT /foodVriation': 'FoodVariationController.update',
  'DELETE /foodVriation': 'FoodVariationController.remove',
  
  // Orders
  'POST /order': 'OrdersController.insert',
  'GET /order': 'OrdersController.get',
  'GET /orders': 'OrdersController.getAll',       
  'PUT /order': 'OrdersController.update',
  'DELETE /order': 'OrdersController.remove',

};

module.exports = privateRoutes;
