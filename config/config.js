const dotenv = require('dotenv');
dotenv.config();

module.exports	=	{
	enviroment: "development",
	development: {
		username: 'apptesting_fastlife',
		password: 'Time@1233',
		database: 'apptesting_fastlife',
		host: '167.86.127.99',
		dialect: 'mysql',
		port: '3306',
		// Use a different storage type. Default: sequelize
		migrationStorage: "json",
		// Use a different file name. Default: sequelize-meta.json
		migrationStoragePath: "sequelize_meta.json",
	},
	test: {
		dialect: "sqlite",
		storage: ":memory:",
	},
	production: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME,
		host: process.env.DB_HOSTNAME,
		port: process.env.DB_PORT,
		dialect: process.env.DB_DIALECT,
		migrationStorage: "json",

		// Use a different file name. Default: sequelize-meta.json
		migrationStoragePath: "sequelize_meta.json",
	},
	app: {
		name: 'proteam',
		baseUrl: 'http://localhost:5043',
	}
};
