const dotenv = require('dotenv');
dotenv.config();

const development = {
    username: 'apptesting_fastlife',
		password: 'Time@1233',
		database: 'apptesting_fastlife',
		host: '167.86.127.99',
  dialect: 'mysql',
};

const testing = {
  username: 'apptesting_fastlife',
  password: 'Time@1233',
  database: 'apptesting_fastlife',
  host: '167.86.127.99',
  dialect: 'mysql',
};

const production = {
  database: process.env.DB_NAME,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOSTNAME,
  dialect: process.env.DB_DIALECT,
};

module.exports = {
  development,
  testing,
  production,
};
